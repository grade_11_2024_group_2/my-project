[team](./team.md)

[projectidea](./Project_Idea.md)

[terminologies](./terminologies.md)

# Artificial Pancreatic Stimulation

## Team
-  Jigme Seldon: Programmer & Designer   

- Pema Yangchen: Circuit builder

- Rinchen Yoedsel Pelzom: Reseacher

- Tandin Wangmo: Tester

## Project Concept
The project concept is to develop an artificial pancreas system using Arduino technology, a glucose monitoring sensor, and an insulin delivery pump.
This project helps students learn by letting them use technology to create an artificial pancreas. They get hands-on experience with Arduino, glucose sensors, and insulin pumps, learning how these devices work together in healthcare.

**Team Outcomes:**

- Our goal is to create a functional artificial pancreas with Arduino, glucose sensors, and insulin pumps, developing technical skills, solving challenges together, and growing collectively.



