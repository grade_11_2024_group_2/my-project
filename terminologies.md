
[team](./team.md)

[projectidea](./Project_Idea.md)

[index](./index.md)

# Terminology Recording

## Key Terms and Concepts

Glucose - Main type of sugar in the blood and is the major source of energy for the body's cells. Glucose comes from the foods we eat or the body can make it from other substances.

Insulin - A hormone that lowers the level of glucose (a type of sugar) in the blood. It's made by the beta cells of the pancreas and released into the blood when the glucose level goes up, such as after eating.

Insulin pump - An insulin pump is a medical device used to administer insulin continuously to individuals with diabetes, providing precise insulin dosages throughout the day to manage blood sugar levels effectively.

Conductivity sensor -  A sensor used to measure the conductivity of various solutions such as ultrapure water, pure water, drinking water, sewage, etc., or the concentration of the overall ion of water samples in the field of laboratory, industrial production, and detection.





