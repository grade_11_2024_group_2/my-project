## Progress
Make a box to keep our things in save place.

<img src="./unnamed (1).jpg" width="300">

First we checked the components to ensure they were working properly. While testing them, we found it diffcult to use the multimeter.

As a result, we decided to learn the basics of multimeter usage so that we can improve our skills and use it more effectively in our project.


<img src="./unnamed.jpg" width="300">

We attempted to make a conductivity sensor using aluminum foil and foam, but unfortunately, it didn't work out as expected. During the experiment, we used filtered water and lemon squash juice as our test liquids. However, we struggled to get clear readings from the multimeter because the foam absorbed the water, interfering with the accuracy of the measurements.

On a different note, we made significant progress with our insulin pump project by successfully creating the impeller. The next step is to design and fabricate a cover to protect the impeller and other internal components, which will help us finalize the project.

<img src="./Screenshot%202024-06-13%20003149.png" width="350">

This is the type result we expect.

<img src="./Screenshot%202024-06-11%20123812.png" width="350">

In our next attempt, we used polystyrene foam as the material. This choice proved to be much better than our previous attempts, as the foam offered the right balance of strength and lightweight properly. With the mechanical design performing well, our focus has now shifted to improving the circuit and coding. 

<img src="./93319046-closeup-white-polystyrene-foam-on-the-cardboard-polystyrene-foam-is-cushioning-material-in-packaging.jpg" width="300">

We need to optimize the control system to ensure precise regulation of the insulin flow. 

<img src="./unnamed (2).jpg" width="200">

We asked Maam Tashi if we could get some distilled water for our project , but she mentioned that there wasnt much left in the lab. 
Since we needed it for our experiment , we decided to search online for how to make distilled water at home. We found a few methods and realized that with basic equipment, we could produce our own distilled water. 
This would not only solve our immediate problem but also tech us a valuable skill for the future experiments.

[YouTube Video](https://youtu.be/VHZitT0-fCY?si=nLDiIpJnzI4QlOPj)

<img src="./Screenshot 2024-08-22 100746.png" width="300">

We designed the insulin pump using Fusion 360. The software allowed us to create a precise 3D model, ensuring that all components fit together correctly. By leveraging the advanced tools in Fusion, we were able to simulate how the pump would function in real life. After designing the main body, we focused on the smaller components like the impeller. We also considered how the cover would be attached to protect the internal parts. 

<img src="./Screenshot 2024-08-22 103426.png" width="300">
 




