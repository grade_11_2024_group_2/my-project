# Team Information

[index](./index.md)

[projectidea](./Project_Idea.md)

[terminologies](./terminologies.md)

## Team member
### 

<img src="./image/IMG_9256.JPG" width="300">

Name: Pema Yangchen

Age: 16

Hobbies: To watch movies, play video games, spent time on social media, reading books and exploring new things. 

Learning goal: I hope to learn more about how pancreas help our body and mainly I want to know more about biotechnology and how it works. While in the progress of completing the project I hope to learn about technology materials and their functions. For this project I would try to research on the circuit building and to build the required circuit for the project. 

<img src="./image/DSC08920.JPG" width="300">
 

Name: Jigme Seldon

Age: 17

Hobbies: Spending time with my friends, reading books and manhwa, watching kdrama and anime and I enjoying listening to music.


Learning goal: I hope to learn about how biotechnology works. I hope to enchance my skil on programming and on hand on experiments. I will try to contribut in this project by learning more about programming so that we can make it successful.    

<img src="./image/Rinchen.JPG" width="300">


Name: Rinchen Yoedsel Pelzom

Age: 16

Hobbies: My hoddies include, music, food, reading, poetry and literature, travelling, as well as cooking and baking.

Learning goal: 

My learning goals involve improving my coding skills, getting better at working in teams, learning to use new software, and improving my understanding on how circuits work. I want to get better at basic coding languages like JavaScript and Python. I also want to work better with others in group projects and activities. I'm excited to explore new software that can help me in my studies and hobbies. And I'm curious to learn about how electronic circuits are built and how they function.


<img src="./image/unnamed.jpg" width="300">

Name: Tandin Wangmo

Age: 17

Hobbies: My hobbies include reading books, Wattpad stories, and blogs, learning languages, watching dramas and movies, cooking, photography,listening to music, spending quality time with my family, and trekking and traveling to different places.

Learning goal:My learning goal is to simultaneously enhance my programming skills and delve into the field of biotechnology. I aspire to deepen my understanding of biotechnology, focusing on areas such as  bioinformatics.

Team Learning Strategies

- The person building circuits will focus on making and putting together hardware parts, also teaching the team about circuits.
- The tester will check how well everything works, work with the programmer on testing, and share what they see, learning about how hardware works and the science behind it.
- The programmer will write software codes, work closely with the circuit builder and tester, teach coding, and understand hardware and science as the project goes on.
- The designer will design the things which are need to be designed and also teach the other what you have leart. 



 
