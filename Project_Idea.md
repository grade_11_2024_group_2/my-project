
[team](./team.md)                      

[terminologies](./terminologies.md)

[index](./index.md)


# Project Idea 

## Introduction

Our project focuses primarily on Type 1 diabetes,a condition where the pancreas fails to produce insulin due to an autoimmune response, causing high blood glucose levels. It requires lifelong insulin therapy to manage, with dosage influenced by factors such as diet, exercise, stress, and illness.
 


Our project is to mainly see how insulin delivery happens in response to blood glucose level.
We will be making an artifical pancreas. We will use distilled water as blood,tap water as insulin, blood glucose level as voltage. 

Distilled water is not very conductive.So,we can represent as blood with high sugar content which will cause the circuit to output a high voltage.

Tap water is more conductive so when we add it to distilled water the conductivity will increase.That's why we can use it as isulin. 

#### Rough sketch of our simulation

![](./image/WIN_20240419_16_01_14_Pro.jpg)


### How will the Circuit Work?

Figure 3 shows the circuit you will build in this project. It has two main parts. First, a voltage divider measures the conductivity of the water. The voltage divider consists of one fixed resistor (R1) and a pair of electrodes immersed in water (R2). The input voltage (Vin) is the 5 V supply from the Arduino. The output voltage (Vout) is measured by one of the Arduino's analog (continuously variable) inputs. A voltage divider's output voltage is determined by Equation 1:

<img src="./image/Screenshot 2024-06-20 205705.png" width="150">

Next, the circuit has a pump that is controlled by one of the Arduino's digital (on/off) pins and a transistor. The transistor acts like an electronic valve that controls the current flowing through the pump. A transistor is necessary because the Arduino's digital pins can only supply a small amount of electrical current, about 20 milliamps (mA), while the pump requires about 500 mA. The transistor allows the pump to draw its power directly from the Arduino's 5 V supply, which can provide much more current than the individual digital pins.

<img src="./image/Screenshot 2024-06-20 205721.png" width="350">
      
### Purpose

To create a lifelike simulation of Type 1 diabetes where users learn to give insulin to lower high blood sugar levels, helping them improve their skills in managing the condition.
 
### Components List

Components Required for Artificial Pancreas System:

1. Arduino Metro

2. Conductivity Sensor

<img src="./image/Screenshot (9).png" width="300">
      
3. Insulin Pump

 <img src="./image/Screenshot 2024-06-20 201959.png" width="150">
     
4. IRF520 MOSFET

5. 100 kΩ Resisto

6. Jumper Wires

7. Alligator Clips

8. Breadboard

9. USB Cable

10. Containers

11. Tubing
    
12. Distilled water

13. Tap water

14. Multimeter


### Conclusion

The project aims to create an artificial pancreas using Arduino, a sensor to measure glucose levels, and a pump to deliver insulin.
